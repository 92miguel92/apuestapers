//
//  ViewController.swift
//  ApuestasPersistencia
//
//  Created by Master Móviles on 11/02/2017.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var apuesta: Int?
    var total: Int?
    @IBOutlet weak var numeroAleatorio: UILabel!
    @IBOutlet weak var totalCredito: UILabel!
    @IBAction func parButton(_ sender: UIButton) {
        apuesta = Int(apuestaTV.text!)
        total = Int(totalCredito.text!)
        let numeroAl = getRandom()
        if(apuestaTV.text != nil){
            if(numeroAl%2 == 0){
                total = total!+apuesta!
            }else{
                total = total!-apuesta!
            }
            totalCredito.text = String(describing: total!)
        }else{
            print("Añade una apuesta")
        }
    }
    @IBAction func imparButton(_ sender: UIButton) {
        apuesta = Int(apuestaTV.text!)
        total = Int(totalCredito.text!)
        let numeroAl = getRandom()
        if(apuestaTV.text != nil){
            if(numeroAl%2 != 0){
                total = total!+apuesta!
            }else{
                total = total!-apuesta!
            }
            totalCredito.text = String(describing: total!)
        }else{
            print("Añade una apuesta")
        }
    }
    @IBOutlet weak var apuestaTV: UITextField!
    
    func getRandom() -> Int{
        let randomNum:UInt32 = arc4random_uniform(100) // range is 0 to 99
        
        let randomTime:TimeInterval = TimeInterval(randomNum)
        
        let random = round(randomTime)
        numeroAleatorio.text = String(Int(random))
        
        return Int(random)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector:#selector(self.vamosABackground), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        let urls = FileManager.default.urls(for:.documentDirectory, in:.userDomainMask)
        if(urls.count>0) {
            let docDir = urls[0]
            print("El directorio 'Documents' es \(docDir)")
            var NSdiccionario : NSDictionary?
            print("PATH: \(docDir.path+"Credito.plist")")
            NSdiccionario = NSDictionary(contentsOfFile: docDir.path+"/Credito.plist")
            if let unwrappedDict = NSdiccionario {
                if let credito = unwrappedDict["credito"],
                    let apuesta = unwrappedDict["apuesta"] {
                    totalCredito.text = String(describing: credito)
                    apuestaTV.text = String(describing: apuesta)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func vamosABackground(){
        print("Nos notifican que vamos a background");
        let urls = FileManager.default.urls(for:.documentDirectory, in:.userDomainMask)
        let diccionario : [String : Any] = [
            "credito" : totalCredito.text!,
            "apuesta" : apuestaTV.text!
        ]
        if(urls.count>0) {
            let docDir = urls[0]
            print("El directorio 'Documents' es \(docDir)")
            let urlPlist = docDir.appendingPathComponent("Credito.plist")
            let NSdiccionario = diccionario as NSDictionary
            NSdiccionario.write(to: urlPlist, atomically: true)
        }
        else {
            print("error al buscar el directorio 'Documents'")
        }
    }

}

